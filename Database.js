var DS = Ti.Filesystem.separator;

function Database(databaseName, path){
	this.databaseName = (databaseName == undefined) ? 'database' : databaseName;
	var isPath = (path == undefined) ? false : true;
	this.debug = true;
	this.debug_logs = [];
	this.tablePrefix = "";
	this.tableName = null;
	
	if(isPath){
        //var path = Ti.Filesystem.externalStorageDirectory + "db" + DS + this.databaseName + ".sql";
        this.sql = Ti.Database.install("mydb1", path);
	}else{
		this.sql = Titanium.Database.open(this.databaseName);
	}
}

Database.prototype.setTable = function(tableName){
	this.tableName = tableName;
};

/**
 * Ha van tábla prefix akkor a táblanév elé odafüzzük
 * @memberOf lib.LocalData
 * @method tableName
 * @param {string} name - Tábla neve
 * @return String
 */
Database.prototype.tableName = function(name) {
    return this.tablePrefix + name;
};

/**
 * SQL paramcs futtatása
 * @memberOf lib.LocalData
 * @method execute
 * @param {string} query - SQLite query paramcs
 * @return boolean
 */
Database.prototype.execute = function(query) {
    if (this.debug)
        Ti.API.info("Database [execute]: " + query);
    try {
        return this.sql.execute(query);
    } catch(e) {
        return false;
    }
};

/**
 * Tötöljuk a tábla tartalámát
 * @memberOf lib.LocalData
 * @method truncate
 * @param {string} tableName - tábla név
 * @return void
 */
Database.prototype.truncate = function() {
    tableName = this.tableName(this.tableName);
    var query = "DELETE FROM " + tableName;
    this.execute(query);
};

/**
 * Töröljünk a sql kapcsolatot
 * @memberOf lib.LocalData
 * @method remove
 * @return void
 */
Database.prototype.remove = function() {
    this.sql.remove();
};

/**
 * Töröljük az adatbázis táblát
 * @memberOf lib.LocalData
 * @method drop
 * @param {string} tableName - Tábla név
 * @return void
 */
Database.prototype.drop = function() {
    tableName = this.tableName(this.tableName);
    var query = "DROP TABLE IF EXISTS " + tableName;
    this.execute(query);
};

/**
 * Bezárjuk a sql kapcsolatot
 * @memberOf lib.LocalData
 * @method close
 * @return void
 */
Database.prototype.close = function() {
    this.sql.close();
};

/**
 * Convertáljuk a visszakapott adatbázis sort ha van benne json akkor az object-t csinálunk belőle
 * @memberOf lib.LocalData
 * @method toObject
 * @param {execute} cursor - Amikor visszaválaszol a sql a nyers fel nem dolgozott adattal - > execute
 * @param {string} formatCase - SnakeCase vagy CamelCase
 * @return object
 */
Database.prototype.toObject = function(cursor, formatCase) {
    var object = {};

    for (var i = 0; i < cursor.getFieldCount(); i++) {
        var key = cursor.getFieldName(i);
        var value = cursor.fieldByName(cursor.getFieldName(i));
        key = formatCase ? key.convertCase(formatCase) : key;
        if ( typeof value == "string") {
            if (value.length > 0 && (value.charAt(0) == '[' || value.charAt(0) == '{')) {
                value = value.tryJSONConversion();
            }
        }

        object[key] = value;
    }
    return object;
};

/**
 * Itt futtatjuk a SELECT-es sql query-t
 * @memberOf lib.LocalData
 * @method query
 * @param {string} query - SQLite query parancs
 * @return ArrayObject
 */
Database.prototype.query = function(query) {
    var cursor = this.execute(query);
    var allRows = this.toArray(cursor);
    cursor.close();
    return allRows;
};

/**
 * Az adatbázis által visszaadtott cursort elősszür Array-be majd azok elemibe Objectumot teszünk
 * @memberOf lib.LocalData
 * @method toArray
 * @param {cursor} cursor - Amikor visszaválaszol a sql a nyers fel nem dolgozott adattal - > execute
 * @param {string} formatCase - SnakeCase vagy CamelCase
 * @return ArrayObject
 */
Database.prototype.toArray = function(cursor, formatCase) {
    var array = [];
    while (cursor.isValidRow()) {
        array.push(this.toObject(cursor, formatCase));
        cursor.next();
    }
    return array;
};

/**
 * @typedef ArrayObject
 * @property {string} name - Az oszlop neve
 * @property {string} type - Az oszlop typuse pl.: VARCHAR(10) vagy INT
 * @property {boolean} [isPrimaryKey=false] - Elsődleges kulcs legyen e vagy sem
 *
 * Létrehozunk egy adatbázis táblát
 * @memberOf lib.LocalData
 * @method create
 * @param {string} tableName - Tábla név
 * @param {ArrayObject} data - Tábla elemei
 * @return sql query
 */
Database.prototype.create = function(tableName, data) {
    tableName = this.tableName(tableName);
    var query = "CREATE TABLE IF NOT EXISTS " + tableName + " (";
    var primaryKeys = [];
    for (var i = 0; i < data.length; i++) {
        var column = data[i];

        var name = column.name;
        var type = column.type;
        var pk = column.isPrimaryKey;
        var ai = column.isAutoIncrement;
        var nullEnabled = column.nullEnabled === false ? false : true;

        query += name + " " + type + " " + ( nullEnabled ? "" : "NOT NULL ") + (i < data.length - 1 ? ", " : " ");
        if (pk)
            primaryKeys.push(name);
    }
    if (primaryKeys.length > 0) {
        query += ", PRIMARY KEY ";
        query += "(";
        for (var i = 0; i < primaryKeys.length; i++)
            query += primaryKeys[i] + (i < primaryKeys.length - 1 ? ", " : " ");
        query += ")";
    }
    query += ")";

    this.execute(query);
    return query;
};

exports.Database = Database;