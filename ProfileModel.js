var AbstractModel = require('./AbstractModel.js');

function Profile() {
    AbstractModel.call(this);
    this.useTable = 'profiles';
}

Profile.prototype = new AbstractModel();
Profile.prototype.constructor = Profile;
module.exports = Profile;





